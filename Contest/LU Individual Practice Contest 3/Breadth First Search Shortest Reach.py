# https://www.hackerrank.com/challenges/bfsshortreach

T = int(input())
while T:
    T -= 1
    adj = []
    n, m = map(int, input().split())
    for i in range(n+3):
        adj.append([])
    for i in range(m):
        x, y = map(int, input().split())
        adj[x].append(y)
        adj[y].append(x)
    s = int(input())
    vis = [False] * (n+3)
    dist = [0] * (n+3)
    q = []
    q.append(s)
    dist[s] = 0
    vis[s] = True
    while len(q) != 0:
        u = q[0]
        q.pop(0)
        sz = len(adj[u])
        for i in range(sz):
            v = adj[u][i]
            if vis[v] == False:
                vis[v] = True
                dist[v] = dist[u] + 6
                q.append(v)
    for i in range(1, n + 1):
        if i != s:
            if dist[i] == 0:
                print("-1 ", end="")
            else:
                print(dist[i], end=" ")
    print("")