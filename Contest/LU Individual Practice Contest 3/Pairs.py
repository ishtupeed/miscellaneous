n, k = map(int, input().split())
s = [int(x) for x in input().split()]
ans = 0
s.sort()
m = {}
for i in range(n):
    if s[i] in m or s[i] - k in m or s[i] + k in m:
        ans += 1
    m[s[i]] = 1
print(ans)
