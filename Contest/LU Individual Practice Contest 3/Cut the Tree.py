n = int(input())
val = list(map(int, input().split()))
t = sum(val)

edges = [[] for i in range(n)]

for j in range(n - 1):
    u, v = map(int, input().split())
    u -= 1
    v -= 1
    edges[u].append(v)
    edges[v].append(u)

totVal = [0] * n
q = {i for i in range(n) if len(edges[i]) == 1}
while len(q):
    for i in q:
        totVal[i] += val[i]
        if len(edges[i]):
            edges[edges[i][0]].remove(i)
            totVal[edges[i][0]] += totVal[i]
    q = {edges[i][0] for i in q if len(edges[i]) and len(edges[edges[i][0]]) == 1}

ans = abs(t - 2 * totVal[0])
for i in range(1, n):
    k = abs(t - 2 * totVal[i])
    if k < ans:
        ans = k

print(ans)
