n, m = map(int, input().split())
for i in range(n):
    s = input()
    ans = ""
    for j in range(m):
        if s[j] == '.':
            if (i + j) & 1 == 1:
                ans += "W"
            else:
                ans += "B"
        else:
            ans += s[j]
    print(ans)
